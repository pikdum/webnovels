import json
from flask import Flask, request, Response
from flask_cors import CORS, cross_origin
from waitress import serve
app = Flask(__name__)
CORS(app)

@cross_origin
@app.route("/<int:book>/<int:chapter>", methods=['POST'])
def write_chapter(book, chapter):
    filename = "/data/%d_%d.txt" % (book, chapter)
    data = request.data.replace("<script>", "&lt;script&gt;").replace("</script>", "&lt;script&gt;")
    f = open(filename, "w")
    f.write(data);
    f.close()
    return "success!"

@app.route("/health")
def health():
    d = {
        "status": "success"
    }
    r = Response(json.dumps(d, sort_keys=True))
    r.headers['Content-Type'] = 'application/json'
    return r, 200

if __name__ == "__main__":
    serve(app)
